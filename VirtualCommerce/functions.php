<?php

// Register scripts for v-commerce page
function vcommerce_enqueue_scripts() {
	// Only enqueue scripts on the v-commerce page
	if (!is_page('v-commerce')) {
		return;
	}

	$path_prefix = get_stylesheet_directory_uri() . '/scripts/';

	wp_enqueue_script('vc-aframe', $path_prefix . '/aframe-v1.3.0.min.js');
	wp_enqueue_script('vc-aframe-environment', $path_prefix . '/aframe-environment-component.min.js');
	wp_enqueue_script('vc-peer', $path_prefix . '/peerjs.min.js');
	wp_enqueue_script('vc-vcommerce', $path_prefix . '/vcommerce.js');
}
add_action('wp_enqueue_scripts', 'vcommerce_enqueue_scripts');


// Register style for inside iframe
function vcommerce_enqueue_styles() {
	if (!is_product()) {
		return;
	}

	wp_enqueue_style('vc-popup', get_stylesheet_directory_uri() . '/styles/popup.css');
}
add_action('wp_enqueue_scripts', 'vcommerce_enqueue_styles');


// Register shortcode for v-commerce page
function vcommerce_register_shop_shortcode() {
	add_shortcode('vcommerce', 'vcommerce_render_shop_shortcode');
}
add_action('init', 'vcommerce_register_shop_shortcode');


// Allow glTF files for upload
function vcommerce_allow_gltf_upload($mimes) {
	$gltf_mime_types = [
		'glb'   => 'application/octet-stream',
		'gltf'  => 'model/gltf+json',
		'bin'   => 'application/octet-stream',
		'glsl'  => 'text/plain',
		'vert'  => 'text/x-glsl',
		'vsh'   => 'text/x-glsl',
		'gsh'   => 'text/x-glsl',
		'frag'  => 'text/x-glsl',
		'glsl'  => 'text/x-glsl',
		'shader'=> 'text/x-glsl',
	];

	foreach ($gltf_mime_types as $ext => $type) {
		$mimes[$ext] = $type;
	}

	return $mimes;
}
add_filter('upload_mimes', 'vcommerce_allow_gltf_upload');


// Register ACF for product (glTF)
function vcommerce_register_product_fields() {
	acf_add_local_field_group([
		'key' => 'vcommerce_product_settings',
		'title' => 'V-Commerce Einstellungen',
		'fields' => [
			[
				'key' => 'field_model_file',
				'label' => 'glTF Model Datei',
				'name' => 'model_file',
				'type' => 'file',
			],
			[
				'key' => 'field_3d_position',
				'label' => 'Position',
				'name' => '3d_position',
				'type' => 'text',
				'placeholder' => '0 0 0',
			],
			[
				'key' => 'field_3d_rotation',
				'label' => 'Rotation',
				'name' => '3d_rotation',
				'type' => 'text',
				'placeholder' => '0 0 0',
			],
			[
				'key' => 'field_3d_scale',
				'label' => 'Skalierung',
				'name' => '3d_scale',
				'type' => 'text',
				'placeholder' => '1 1 1',
			]
		],
		'location' => [
			[
				[
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'product',
				],
			],
		],
	]);
}
add_action('acf/init', 'vcommerce_register_product_fields');


// Render v-commerce shortcode
function vcommerce_render_shop_shortcode() {
	// Get all Products
	$products = wc_get_products([
		'status' => 'publish',
		'limit' => -1,
	]);
?>
<a-scene environment cursor="rayOrigin: mouse;" raycaster="objects: .product">
	<a-assets id="asset-library">
<?php
	foreach ($products as $product) {
		// Check if we have a 3D model
		$model = get_field('field_model_file', $product->get_id());
		if ($model) {
			echo '<a-asset-item id="asset-product-'.$product->get_id().'" src="'.$model['url'].'"></a-asset-item>';
		}
	}
?>
		<a-asset-item id="v-robo" src="<?php echo get_stylesheet_directory_uri(); ?>/v-robo.glb"></a-asset-item>
	</a-assets>
	<a-entity id="camera-rig">
		<a-camera id="camera" listener=""></a-camera>
	</a-entity>
	<a-entity id="peerEntity" visible="false">
		<a-entity id="peerEntityHead" position="0 1.6 0">
			<a-box id="peerEntityHeadVideo" scale="0.5 0.6 0.2"></a-box>
			<a-box id="peerEntityHeadBlocker" position="0 0 0.01" scale="0.51 0.61 0.21"></a-box>
		</a-entity>
		<a-entity id="peerEntityBody" gltf-model="#v-robo" position="0 0 0" rotation="0 90 0" scale="0.4 0.4 0.4"></a-entity>
	</a-entity>

	<a-entity highlight>
<?php
	foreach ($products as $product) {
		$position = get_field('field_3d_position', $product->get_id());
		if (!$position) {
			$position = '0 0 0';
		}

		$rotation = get_field('field_3d_rotation', $product->get_id());
		if (!$rotation) {
			$rotation = '0 0 0';
		}

		$scale = get_field('field_3d_scale', $product->get_id());
		if (!$scale) {
			$scale = '1 1 1';
		}
		echo '<a-entity class="product" product-id="'.$product->get_id().'" id="product-'.$product->get_id().'" gltf-model="#asset-product-'.$product->get_id().'" position="'.$position.'" rotation="'.$rotation.'" scale="'.$scale.'"></a-entity>';
	}
?>
	</a-entity>
	<a-entity environment="preset: tron; dressing: towers; dressingAmount: 50"></a-entity>
</a-scene>
<div id="popup-overlay"></div>
<div id="link-overlay"></div>
<?
}