document.addEventListener('DOMContentLoaded', init, false);

let scene = null;
let video = null;
let peerVideo = null;
let selfPosition = null;
let selfRotation = null;
let peerConnection = null;

function init() {
	displayMessage('');

	scene = document.querySelector('a-scene');
	if (scene.hasLoaded) {
		initWebRTC();
	} else {
		scene.addEventListener('loaded', initWebRTC);
	}
}

async function initWebRTC() {
	displayMessage('Auf lokale Kamera warten...');
	try {
		// Start our own video
		const mediaStream = await navigator.mediaDevices.getUserMedia({video: true , audio: true});

		// Connect to PeerJS messaging server
		displayMessage('Mit dem Server verbinden...');
		const peer = new Peer();

		peer.on('open', function(id) {
			if (!window.location.hash) {
				// Create a "room" and display link to share
				displayMessage(`Link teilen: ${window.location}#${id}`);

				// Change our camera
				const camera = document.getElementById('camera');
				camera.setAttribute('position', {
					x: 0,
					y: 1.6,
					z: 4
				});
			} else {
				// Join a "room" and initiate call
				displayMessage('Anruf starten...');
				const peerId = window.location.hash.replace('#', '');
				const call = peer.call(peerId, mediaStream);
				call.on('stream', peerStreamConnected);

				// Start Data Connection (for peer position)
				const connection = peer.connect(peerId);
				peerConnection = connection;
				peerDataConnected(connection);

				// Change our camera
				const camera = document.getElementById('camera');
				camera.setAttribute('position', {
					x: 0,
					y: 1.6,
					z: -4
				});
				camera.components['look-controls'].yawObject.rotation.y = Math.PI;
			}
		});

		// Wait for call from other peer
		peer.on('call', function(call) {
			call.answer(mediaStream);
			call.on('stream', peerStreamConnected);
		});

		// Wait for data connection (position)
		peer.on('connection', function(connection) {
			peerConnection = connection;
			peerDataConnected(connection);
		});

		// Error handling
		peer.on('disconnected', function() {
			displayMessage('Neuer Versuch...');
			peerConnection = null;
			peer.reconnect();
		});

		peer.on('error', function(error) {
			displayMessage(`Fehler: ${error.type}.<br />Zum Neustarten Seite neu laden.`);
			peerConnection = null;
		});
	} catch (error) {
		displayMessage(error.message);
	}
}

function peerStreamConnected(stream) {
		// Create video elements in asset library
	const assetLibrary = document.getElementById('asset-library');

	peerVideo = document.createElement('video');
	peerVideo.setAttribute('id', 'peerVideo');
	peerVideo.srcObject = stream;
	assetLibrary.appendChild(peerVideo);
	document.getElementById('peerEntityHeadVideo').setAttribute('src', '#peerVideo');

	peerVideo.play();
	document.getElementById('peerEntity').setAttribute('visible', true);

	displayMessage('');
}

function peerDataConnected(connection) {
	// Data Types:
	// 1: Camera Updates (User moves)
	// 2: Highlight Item (Mouse Enter)
	// 3: De-Highlight Item (Mouse Leave)
	// 4: Open Popup (Click)
	// 5: Close Popup (Click on X)
	connection.on('data', function(data) {
		if (data.t === 1) {
			document.getElementById('peerEntity').setAttribute('position', {...data.p, y: 0});
			document.getElementById('peerEntity').setAttribute('rotation', {...data.r, x: 0, z: 0});
			document.getElementById('peerEntityHead').setAttribute('rotation', {...data.r, y: 0});
		} else if (data.t === 2) {
			highlight(data.i);
		} else if (data.t === 3) {
			dehighlight(data.i);
		} else if (data.t === 4) {
			openPopup(data.i);
		} else if (data.t === 5) {
			closePopup();
		}
	});

	connection.on('open', function() {
		setInterval(function() {
			connection.send({
				t: 1,
				p: {
					x: selfPosition.x,
					y: selfPosition.y,
					z: selfPosition.z,
				},
				r: {
					x: selfRotation.x,
					y: selfRotation.y,
					z: selfRotation.z,
				},
			});
		}, 33);
	});
}

// Displays a message at the top of the page.
// Empty string hides the message
function displayMessage(message) {
	const linkOverlay = document.getElementById('link-overlay');
	linkOverlay.innerHTML = `<p>${message}</p>`;

	if (message.length === 0) {
		linkOverlay.style.display = 'none';
	} else {
		linkOverlay.style.display = 'block';
	}
}


// Shop Functions
function highlight(productId) {
	const product = document.getElementById(productId);
	product.object3D.scale.multiplyScalar(1.2);
}

function dehighlight(productId) {
	const product = document.getElementById(productId);
	product.object3D.scale.multiplyScalar(0.833333333333);
}

function openPopup(productId) {
	const popup = document.getElementById('popup-overlay');
	popup.innerHTML = `<a id="popup-overlay-close">X</a><iframe src="/?post_type=product&p=${productId}" frameborder="0" onload="jQuery(document.body).trigger('wc_fragment_refresh');"></iframe>`;
	popup.style.display = 'block';

	const closeButton = document.getElementById('popup-overlay-close');
	closeButton.addEventListener('click', () => {
		closePopup();

		if (peerConnection) {
			peerConnection.send({
				t: 5
			});
		}
	});
}

function closePopup() {
	const popup = document.getElementById('popup-overlay');
	popup.style.display = 'none';
}


// Aframe components
AFRAME.registerComponent('listener', {
	tick: function() {
		selfPosition = this.el.getAttribute('position');
		selfRotation = this.el.getAttribute('rotation');
	}
});


AFRAME.registerComponent('highlight', {
	init: function () {
		this.onClick = this.onClick.bind(this);
		this.onMouseEnter = this.onMouseEnter.bind(this);
		this.onMouseLeave = this.onMouseLeave.bind(this);

		const products = this.el.querySelectorAll('.product');
		for (const product of products) {
			product.addEventListener('mouseenter', this.onMouseEnter);
			product.addEventListener('mouseleave', this.onMouseLeave);
			product.addEventListener('click', this.onClick);
		}
	},

	onClick: function (event) {
		openPopup(event.target.getAttribute('product-id'));

		if (peerConnection) {
			peerConnection.send({
				t: 4,
				i: event.target.getAttribute('product-id')
			});
		}
	},

	onMouseEnter: function (event) {
		highlight(event.target.id);

		if (peerConnection) {
			peerConnection.send({
				t: 2,
				i: event.target.id
			});
		}
	},

	onMouseLeave: function (event) {
		dehighlight(event.target.id);

		if (peerConnection) {
			peerConnection.send({
				t: 3,
				i: event.target.id
			});
		}
	},
});