document.addEventListener('DOMContentLoaded', init, false);

let scene = null;
let video = null;
let peerVideo = null;
let selfPosition = null;
let selfRotation = null;

function init() {
	displayMessage('Waiting for local camera');

	scene = document.querySelector('a-scene');
	if (scene.hasLoaded) {
		initWebRTC();
	} else {
		scene.addEventListener('loaded', initWebRTC);
	}
}

async function initWebRTC() {
	try {
		// Create video elements in asset library
		const assetLibrary = document.getElementById('asset-library');

		peerVideo = document.createElement('video');
		peerVideo.setAttribute('id', 'peerVideo');
		assetLibrary.appendChild(peerVideo);
		document.getElementById('peerEntity').setAttribute('src', '#peerVideo');

		// Start our own video
		const mediaStream = await navigator.mediaDevices.getUserMedia({video: true , audio: true});

		// Connect to PeerJS messaging server
		displayMessage('Connecting to Server...');
		const peer = new Peer();

		peer.on('open', function(id) {
			if (!window.location.hash) {
				// Create a "room" and display link to share
				displayMessage(`Share Link: ${window.location}#${id}`);
			} else {
				// Join a "room" and initiate call
				displayMessage('Starting Call...');
				const peerId = window.location.hash.replace('#', '');
				const call = peer.call(peerId, mediaStream);
				call.on('stream', peerStreamConnected);

				// Start Data Connection (for peer position)
				const connection = peer.connect(peerId);
				peerDataConnected(connection);

				// Change our camera
				const camera = document.getElementById('camera');
				camera.setAttribute('position', {
					x: 0,
					y: 1.6,
					z: -4
				});
			}
		});

		// Wait for call from other peer
		peer.on('call', function(call) {
			call.answer(mediaStream);
			call.on('stream', peerStreamConnected);
		});

		// Wait for data connection (position)
		peer.on('connection', function(connection) {
			peerDataConnected(connection);
		});

		// Error handling
		peer.on('disconnected', function() {
			displayMessage('Reconnecting to Server...');
			peer.reconnect();
		});

		peer.on('error', function(error) {
			displayMessage(`Received error: ${error.type}.<br />To start again please reload the page.`);
		});
	} catch (error) {
		displayMessage(error.message);
	}
}

function peerStreamConnected(stream) {
	peerVideo.srcObject = stream;
	peerVideo.play();

	displayMessage('');
}

function peerDataConnected(connection) {
	connection.on('data', function(data) {
		document.getElementById('peerEntity').setAttribute('position', data.p);
		document.getElementById('peerEntity').setAttribute('rotation', data.r);
	});

	connection.on('open', function() {
		setInterval(function() {
			connection.send({
				p: {
					x: selfPosition.x,
					y: selfPosition.y,
					z: selfPosition.z,
				},
				r: {
					x: selfRotation.x,
					y: selfRotation.y,
					z: selfRotation.z,
				},
			});
		}, 33);
	});
}

// Displays a message at the top of the page.
// Empty string hides the message
function displayMessage(message) {
	const linkOverlay = document.getElementById('link-overlay');
	linkOverlay.innerHTML = `<p>${message}</p>`;

	if (message.length === 0) {
		linkOverlay.style.display = 'none';
	} else {
		linkOverlay.style.display = 'block';
	}
}


AFRAME.registerComponent('listener', {
	tick: function() {
		selfPosition = this.el.getAttribute('position');
		selfRotation = this.el.getAttribute('rotation');
	}
});
