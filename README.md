# Virtual-Commerce

## Description
Masterarbeit zum Thema Virtual-Commerce
Prototyp fuer einen Virtual-Commerce-Shop basierend auf WordPress mit WooCommerce. 
Im Ordner "WebRTCxWebVR" sind alle JavaScript Dateien und das index.html fuer die Erstellung einer WebRTC Anwendung in einer WebVirtual Reality zu finden. 
Im Ordner "VirtualCommerce" sind alle Dateien, die fuer das Erstellen eines Virtual-Commerce-Shops mit WordPress und dem Plugin WooCommerce als Basis, gesammelt. 

## Installation
Um die Dateien in diesem Repository verwenden zu koennen, muss WordPress auf einem Server installiert werden. Um das Gestalten des Webshops einfacher zu machen, wird das Theme "Flatsome" verwendet, dieses kann ueber den unten angefuehrten Link erworben werden. Im naechsten Schritt muss das Plugin WooCommerce installiert werden. Das kann im Backend von WordPress erledigt werden. Zusaetzlich wird das Plugin GermanMarket empfohlen, um den Shop rechtssicher zu gestalten. Nach dem Erstellen des Flatsome Child-Themes, koennen die Dateien dieses Repositories heruntergeladen und ueber FTP in den Flatsome Child-Theme Ordner wieder hochgeladen werden. 
Alle Dateien bis auf v-robo.glb sind fuer das problemlose Funktionieren notwendig. v-robo.glb ist ein 3D-Modell und stellt den Koerper der Avatare da. 
Nachdem alle Dateien hochgeladen wurden, muss der eigens definierte Shortcode [vcommerce] auf der gewuenschten Seite in einem Textfeld eingebunden werden.  

## Links 
WordPress: https://wordpress.org/download/ 
Flatsome: https://themeforest.net/item/flatsome-multipurpose-responsive-woocommerce-theme/5484319 
German Market: https://marketpress.de/shop/plugins/woocommerce/woocommerce-german-market/ 

## Visuals
![Screenshot Prototype](https://gitlab.com/schwarz-v/virtual-commerce/-/blob/main/VirtualCommerce/Screenshot_V-Commerce.png)

## Project status
Der Prototyp ist fertig. Designaspekte wurden bei der Umsetzung ausser Acht gelassen. 